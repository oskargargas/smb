package com.oglabs.test_as.activities.geolokalizacja;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oglabs.R;
import com.oglabs.test_as.dao.DatabaseManager;
import com.oglabs.test_as.dao.Location;

import java.util.List;


public class MapActivity extends Activity {

    private GoogleMap map;
    private List<Location> locations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
        locations = DatabaseManager.getLocationDao().loadAll();

        for (Location l : locations) {
            map.addMarker(new MarkerOptions()
                    .snippet(l.getDescription())
                    .position(new LatLng(l.getLatitude(), l.getLongitude()))
            );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
