package com.oglabs.test_as.activities.lab_01

import android.app.Activity
import android.os.Bundle
import android.widget.{LinearLayout, TextView}
import macroid.FullDsl._
import macroid._

/**
 * Created by oskar on 08.10.14.
 */

class SecondActivity extends Activity
with Contexts[Activity] with AutoLogTag {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView {
      getUi {
        l[LinearLayout](
          w[TextView] <~ text("Second Activity")
        ) <~ padding(16 dp)
      }
    }
  }
}
