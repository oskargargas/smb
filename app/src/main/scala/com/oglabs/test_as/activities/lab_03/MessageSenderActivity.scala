package com.oglabs.test_as.activities.lab_03

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.{Button, EditText, Toast}
import com.oglabs.R

/**
 * Created by oskar on 19.11.14.
 */
class MessageSenderActivity extends Activity {

  final val CUSTOM_INTENT_NAME: String = "com.oglabs.CUSTOM_INTENT_NAME"

  var messageTextField: EditText = _
  var sendButton: Button = _

  protected override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_message_sender)

    setUiElements()
    setButtonsOnClickListeners()
  }

  def setUiElements() {
    messageTextField = findViewById(R.id.messageSenderEditText).asInstanceOf[EditText]
    sendButton = findViewById(R.id.messageSenderSendButton).asInstanceOf[Button]
  }

  def setButtonsOnClickListeners() {
    sendButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        if (messageTextField.getText.length() == 0) {
          Toast.makeText(getApplicationContext, "Input some message.", Toast.LENGTH_SHORT).show()
        } else {
          val intent = new Intent(CUSTOM_INTENT_NAME)
          intent.putExtra("message", messageTextField.getText.toString)
          intent.setAction(CUSTOM_INTENT_NAME)

          sendBroadcast(intent)

          messageTextField.setText("")

          Toast.makeText(getApplicationContext, "Sending message.", Toast.LENGTH_SHORT).show()
        }
      }
    })
  }
}
