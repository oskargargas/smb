package com.oglabs.test_as.activities.geolokalizacja

import android.app.{Activity, AlertDialog}
import android.content.{Context, DialogInterface, Intent}
import android.location.{Criteria, LocationManager}
import android.os.Bundle
import android.provider.Settings
import android.view.View.OnClickListener
import android.view.{Menu, MenuItem, View}
import android.widget.AdapterView.OnItemLongClickListener
import android.widget._
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.location.LocationServices
import com.oglabs.R
import com.oglabs.test_as.activities.lab_02
import com.oglabs.test_as.dao.{DatabaseManager, Location}
import macroid.{AutoLogTag, Contexts}

import scala.collection.JavaConverters._
import scala.collection.mutable

/**
 * Created by oskar on 29.10.14.
 */
class LocationsListActivity extends Activity
with Contexts[Activity] with AutoLogTag 
with ConnectionCallbacks with OnConnectionFailedListener {

  private var listView: ListView = _
  private var addLocationButton: Button = _
  private var mapButton: Button = _

  private var locations: mutable.Buffer[Location] = _
  private var listViewArrayAdapter: ArrayAdapter[String] = _

  private var googleApiClient: GoogleApiClient = _
  var isConnected = false

  private var locationManager: LocationManager = _
  private var provider: String = _


  protected override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_locations_list)

    buildGoogleApiClient()
    googleApiClient.connect()

    locationManager = getSystemService(Context.LOCATION_SERVICE).asInstanceOf[LocationManager]
    provider = locationManager.getBestProvider(new Criteria(), false)

    setUiElements()
    setListViewOnClickListeners()

    listViewArrayAdapter = new ArrayAdapter[String](this, android.R.layout.simple_list_item_1)
    listView.setAdapter(listViewArrayAdapter)

    addLocationButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        val rawLocation = locationManager.getLastKnownLocation(provider)

        val location = new Location()
        location.setLatitude(rawLocation.getLatitude)
        location.setLongitude(rawLocation.getLongitude)
        location.setDescription("test")

        //TODO: interfejs do wpisywania opisu i nazwy lokalizacji

        DatabaseManager.getLocationDao.insert(location)
        refreshLocations()
      }
    })

    mapButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        val intent = new Intent(LocationsListActivity.this, classOf[MapActivity])
        startActivity(intent)
      }
    })
  }

  def buildGoogleApiClient() {
    googleApiClient = new GoogleApiClient.Builder(this)
      .addConnectionCallbacks(this)
      .addOnConnectionFailedListener(this)
      .addApi(LocationServices.API)
      .build()
  }

  override def onResume(): Unit = {
    super.onResume()

    enableDisableAddButton()
    refreshLocations()
  }

  def setUiElements() {
    listView = findViewById(R.id.locationsListView).asInstanceOf[ListView]
    addLocationButton = findViewById(R.id.locationsListAddLocationButton).asInstanceOf[Button]
    mapButton = findViewById(R.id.locationsListMapButton).asInstanceOf[Button]

    enableDisableAddButton()
  }

  def enableDisableAddButton(): Unit = {
    if (isConnected && isProviderEnabled) {
      addLocationButton.setEnabled(true)
    } else {
      addLocationButton.setEnabled(false)
    }
  }
  
  def isProviderEnabled: Boolean = {
    val providerEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    if (!providerEnabled) {
      val intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
      startActivity(intent)
    }

    providerEnabled
  }

  def setListViewOnClickListeners() {
    listView.setOnItemLongClickListener(new OnItemLongClickListener {
      override def onItemLongClick(parent: AdapterView[_], view: View, position: Int, id: Long): Boolean = {
        new AlertDialog.Builder(LocationsListActivity.this)
          .setTitle("Delete location")
          .setMessage("Are you sure you want to delete this entry?")
          .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener {
          override def onClick(dialog: DialogInterface, which: Int): Unit = {
            DatabaseManager.getLocationDao.deleteByKey(locations(position).getId)
            refreshLocations()
          }
        })
          .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener {
          override def onClick(dialog: DialogInterface, which: Int): Unit = {
            // Do nothing
          }
        }).setIcon(android.R.drawable.ic_dialog_alert).show()

        true
      }
    })
  }

  private def refreshLocations(): Unit = {
    locations = DatabaseManager.getLocationDao.loadAll().asScala
    listViewArrayAdapter.clear()
    listViewArrayAdapter.addAll(locations.map(l => s"${l.getLatitude}, ${l.getLongitude} - ${l.getDescription}").asJava)
  }

  override def onCreateOptionsMenu(menu: Menu): Boolean = {
    getMenuInflater.inflate(R.menu.menu_main, menu)
    true
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    val id: Int = item.getItemId
    if (id == R.id.action_settings) {
      val intent = new Intent(this, classOf[lab_02.SettingsActivity])
      startActivity(intent)

      return true
    }

    super.onOptionsItemSelected(item)
  }

  //OnConnectionFailedListener
  
  override def onConnected(p1: Bundle): Unit = {
    isConnected = true
    enableDisableAddButton()
  }

  override def onConnectionSuspended(p1: Int): Unit = {
    isConnected = false
  }

  override def onConnectionFailed(p1: ConnectionResult): Unit = {
    isConnected = false
  }
}
