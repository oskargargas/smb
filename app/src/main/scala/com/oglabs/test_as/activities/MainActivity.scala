package com.oglabs.test_as.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.{Menu, MenuItem, View}
import android.widget._
import com.oglabs.R
import com.oglabs.test_as.dao.DatabaseManager
import macroid.{AutoLogTag, Contexts}

import scala.collection.JavaConverters._

/**
 * Created by Oskar Gargas on 15.10.14.
 */
class MainActivity extends Activity
with Contexts[Activity] with AutoLogTag {

  private val viewsList = List("Laboratorium 1", "Laboratorium 2", "Laboratorium 3", "Geolokalizacja")

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_main)

    DatabaseManager.initialize(getApplicationContext)

    val listView = findViewById(R.id.listView).asInstanceOf[ListView]
    listView.setAdapter(new ArrayAdapter[String](this, android.R.layout.simple_list_item_1, viewsList.asJava))

    listView.setOnItemClickListener(new AdapterView.OnItemClickListener {
      override def onItemClick(adapterParent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
        position match {
          case 0 =>
            val intent = new Intent(MainActivity.this, classOf[lab_01.FirstActivity])
            startActivity(intent)
          case 1 => 
            val intent = new Intent(MainActivity.this, classOf[lab_02.ProductsListActivity])
            startActivity(intent)
          case 2 =>
            val intent = new Intent(MainActivity.this, classOf[lab_03.MessageSenderActivity])
            startActivity(intent)
          case 3 =>
            val intent = new Intent(MainActivity.this, classOf[geolokalizacja.LocationsListActivity])
            startActivity(intent)
          case _ =>
            Toast.makeText(getApplicationContext, "Some error.", Toast.LENGTH_SHORT).show()
        }
      }
    })
  }

  override def onCreateOptionsMenu(menu: Menu): Boolean = {
    getMenuInflater.inflate(R.menu.menu_main, menu)

    true
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    val id: Int = item.getItemId
    if (id == R.id.action_settings) {
      val intent = new Intent(this, classOf[lab_02.SettingsActivity])
      startActivity(intent)

      return true
    }

    super.onOptionsItemSelected(item)
  }

}
