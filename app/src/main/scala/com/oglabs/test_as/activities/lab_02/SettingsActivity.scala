package com.oglabs.test_as.activities.lab_02

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.{Toast, EditText, Button}
import com.oglabs.R
import com.oglabs.test_as.Constants

/**
 * Created by oskar on 29.10.14.
 */
class SettingsActivity extends Activity {

  private val fontSizePreferencesKey: String = "font_size"
  private val fontColorPreferencesKey: String = "font_color"

  private var fontSizeTextField:  EditText = _
  private var fontColorTextField: EditText = _

  private var okButton:     Button = _
  private var cancelButton: Button = _

  protected override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_settings)

    setUiElements()
    setButtonsOnClickListeners()
    loadApplicationSettings()
  }

  def setUiElements() {
    fontSizeTextField = findViewById(R.id.settingsFontSizeText).asInstanceOf[EditText]
    fontColorTextField = findViewById(R.id.settingsFontColorText).asInstanceOf[EditText]

    okButton = findViewById(R.id.settingsOkButton).asInstanceOf[Button]
    cancelButton = findViewById(R.id.settingsCancelButton).asInstanceOf[Button]
  }

  def setButtonsOnClickListeners() {
    okButton.setOnClickListener(new OnClickListener {

      override def onClick(v: View): Unit = {
        Toast.makeText(getApplicationContext, "Saved application settings.", Toast.LENGTH_SHORT).show()
        saveApplicationSettings()
        finish()
      }
    })

    cancelButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        finish()
      }
    })
  }

  def loadApplicationSettings() = {
    val sharedPreferences = getSharedPreferences(Constants.applicationSettingsSharedPreferencesName, 0)

    fontSizeTextField.setText(sharedPreferences.getInt(fontSizePreferencesKey, 11).toString)
    fontColorTextField.setText(sharedPreferences.getString(fontColorPreferencesKey, "black"))
  }

  def saveApplicationSettings() = {
    val sharedPreferencesEditor = getSharedPreferences(Constants.applicationSettingsSharedPreferencesName, 0).edit()

    val fontSizeText = fontSizeTextField.getText.toString
    if (fontSizeText.length > 0) {
      sharedPreferencesEditor.putInt(fontSizePreferencesKey, fontSizeText.toInt)
    }

    val fontColorText: String = fontColorTextField.getText.toString
    if (fontColorText.length > 0) {
      sharedPreferencesEditor.putString(fontColorPreferencesKey, fontColorText)
    }

    sharedPreferencesEditor.commit()
  }
}
