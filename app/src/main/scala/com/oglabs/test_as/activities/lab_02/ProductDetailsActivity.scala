package com.oglabs.test_as.activities.lab_02

import android.app.Activity
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget._
import com.oglabs.R
import com.oglabs.test_as.dao.{DatabaseManager, Product}

/**
 * Created by oskar on 29.10.14.
 */
class ProductDetailsActivity extends Activity {
  private var product: Product = _
  private var isNewProduct = false

  private var nameTextField:  EditText = _
  private var priceTextField: EditText = _
  
  private var okButton:     Button = _
  private var cancelButton: Button = _

  protected override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_product_details)

    setUiElements()
    setButtonsOnClickListeners()
    setProductFromIntent()
  }

  def setUiElements() {
    nameTextField = findViewById(R.id.productDetailsNameText).asInstanceOf[EditText]
    priceTextField = findViewById(R.id.productDetailsPriceText).asInstanceOf[EditText]

    okButton = findViewById(R.id.productDetailsOkButton).asInstanceOf[Button]
    cancelButton = findViewById(R.id.productDetailsCancelButton).asInstanceOf[Button]
  }

  def setButtonsOnClickListeners() {
    okButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        if (nameTextField.getText.length() == 0 || priceTextField.getText.length() == 0) {
          Toast.makeText(getApplicationContext, "You have to fill name and price.", Toast.LENGTH_SHORT).show()
        } else {
          product.setName(nameTextField.getText.toString)
          product.setPrice(priceTextField.getText.toString.toDouble)

          try {
            if (isNewProduct) {
              DatabaseManager.getProductDao.insert(product)
            } else {
              DatabaseManager.getProductDao.update(product)
            }

            Toast.makeText(getApplicationContext, "Saved product.", Toast.LENGTH_SHORT).show()
            finish()
          } catch {
            case e: SQLiteConstraintException =>
              Toast.makeText(getApplicationContext, "Product name must be unique. There is already product named `" +
                product.getName + "` in the database.", Toast.LENGTH_SHORT).show()
          }
        }
      }
    })

    cancelButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        finish()
      }
    })
  }

  def setProductFromIntent() {
    val productId = getIntent.getLongExtra("product_id", -1)

    if (productId != -1) {
      product = DatabaseManager.getProductDao.load(productId)
      isNewProduct = false
    } else {
      product = new Product()
      isNewProduct = true
    }

    if (product != null) {
      nameTextField.setText(product.getName)
      priceTextField.setText(product.getPrice.toString)
    }
  }
}
