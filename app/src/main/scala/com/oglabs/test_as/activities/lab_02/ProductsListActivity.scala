package com.oglabs.test_as.activities.lab_02

import android.app.{Activity, AlertDialog}
import android.content.{DialogInterface, Intent}
import android.os.Bundle
import android.view.View.OnClickListener
import android.view.{Menu, MenuItem, View}
import android.widget.AdapterView.{OnItemClickListener, OnItemLongClickListener}
import android.widget._
import com.oglabs.R
import com.oglabs.test_as.activities.lab_02
import com.oglabs.test_as.dao.{DatabaseManager, Product}
import macroid.{AutoLogTag, Contexts}

import scala.collection.JavaConverters._
import scala.collection.mutable

/**
 * Created by oskar on 29.10.14.
 */
class ProductsListActivity extends Activity
with Contexts[Activity] with AutoLogTag {
  private var listView: ListView = _
  private var addProductButton: Button = _

  private var products: mutable.Buffer[Product] = _
  private var listViewArrayAdapter: ArrayAdapter[String] = _

  protected override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_products_list)

    setUiElements()
    setListViewOnClickListeners()

    listViewArrayAdapter = new ArrayAdapter[String](this, android.R.layout.simple_list_item_1)
    listView.setAdapter(listViewArrayAdapter)

    addProductButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        val intent = new Intent(ProductsListActivity.this, classOf[ProductDetailsActivity])
        startActivity(intent)
      }
    })
  }

  override def onResume(): Unit = {
    super.onResume()

    refreshProducts()
  }

  def setUiElements() {
    listView = findViewById(R.id.productsListView).asInstanceOf[ListView]
    addProductButton = findViewById(R.id.productsListAddProductButton).asInstanceOf[Button]
  }

  def setListViewOnClickListeners() {
    listView.setOnItemClickListener(new OnItemClickListener {
      override def onItemClick(adapterParent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
        val intent = new Intent(ProductsListActivity.this, classOf[ProductDetailsActivity])
        intent.putExtra("product_id", products(position).getId)

        startActivity(intent)
      }
    })

    listView.setOnItemLongClickListener(new OnItemLongClickListener {
      override def onItemLongClick(parent: AdapterView[_], view: View, position: Int, id: Long): Boolean = {
        new AlertDialog.Builder(ProductsListActivity.this)
          .setTitle("Delete product")
          .setMessage("Are you sure you want to delete this entry?")
          .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener {
          override def onClick(dialog: DialogInterface, which: Int): Unit = {
            DatabaseManager.getProductDao.deleteByKey(products(position).getId)
            refreshProducts()
          }
        })
          .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener {
          override def onClick(dialog: DialogInterface, which: Int): Unit = {
            // Do nothing
          }
        }).setIcon(android.R.drawable.ic_dialog_alert).show()

        true
      }
    })
  }

  private def refreshProducts(): Unit = {
    products = DatabaseManager.getProductDao.loadAll().asScala
    listViewArrayAdapter.clear()
    listViewArrayAdapter.addAll(products.map(p => p.getName + ",         " + p.getPrice).asJava)
  }

  override def onCreateOptionsMenu(menu: Menu): Boolean = {
    getMenuInflater.inflate(R.menu.menu_main, menu)
    true
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    val id: Int = item.getItemId
    if (id == R.id.action_settings) {
      val intent = new Intent(this, classOf[lab_02.SettingsActivity])
      startActivity(intent)

      return true
    }

    super.onOptionsItemSelected(item)
  }
}
