package com.oglabs.test_as.dao

import android.content.Context

/**
 * Created by oskar on 29.10.14.
 */

class DatabaseManager(private val context: Context) {
  private var daoSession: DaoSession = _

  setupDatabase()

  private def setupDatabase(): Unit = {
    val helper = new DaoMaster.DevOpenHelper(context, "database", null)
    val db = helper.getWritableDatabase

    DaoMaster.createAllTables(db, true)
    daoSession = new DaoMaster(db).newSession()
  }

  def getDaoSession: DaoSession = {
    daoSession
  }
}

object DatabaseManager {
  private var databaseManager: Option[DatabaseManager] = None

  def initialize(context: Context): DatabaseManager = {
    if (databaseManager.isEmpty) {
      databaseManager = Option(new DatabaseManager(context))
      getInstance
    } else {
      throw new Exception("DatabaseManager already initialized.")
    }
  }
  
  def getInstance: DatabaseManager = {
    databaseManager.getOrElse(throw new Exception("You have to initialize DatabaseManager first."))
  }

  def getProductDao  = DatabaseManager.getInstance.getDaoSession.getProductDao
  def getLocationDao = DatabaseManager.getInstance.getDaoSession.getLocationDao
}
