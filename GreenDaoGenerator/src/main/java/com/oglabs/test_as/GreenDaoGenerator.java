package com.oglabs.test_as;

import de.greenrobot.daogenerator.*;

/**
 * Created by Oskar Gargas on 29.10.14.
 */
public class GreenDaoGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(3, "com.oglabs.test_as.dao");

        Entity product = schema.addEntity("Product");

        product.addIdProperty().autoincrement();
        product.addStringProperty("name").unique().notNull();
        product.addDoubleProperty("price").notNull();

        Entity location = schema.addEntity("Location");

        location.addIdProperty().autoincrement();
        location.addDoubleProperty("latitude").notNull();
        location.addDoubleProperty("longitude").notNull();
        location.addStringProperty("description");

        new DaoGenerator().generateAll(schema, args[0]);
    }
}
