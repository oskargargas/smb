package com.oglabs.proj_02.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class MyReceiver extends BroadcastReceiver {
    private static final String CUSTOM_INTENT_NAME = "com.oglabs.CUSTOM_INTENT_NAME";

    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(CUSTOM_INTENT_NAME)) {
            String message = intent.getStringExtra("message");

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            notificationBuilder.setSmallIcon(R.drawable.ic_launcher);
            notificationBuilder.setContentTitle("Message Receiver");
            notificationBuilder.setContentText(message);

            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}
